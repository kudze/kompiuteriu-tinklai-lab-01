#include <stdio.h>
#include <stdarg.h>
#include <winsock2.h>
#include <stdbool.h>
#include <limits.h>

#define MAX_CLIENTS 100
#define BUFFER_LENGTH 1024

#define CLIENT_STATE_IDLE 0

typedef struct {
    bool used;
    SOCKET socket;
    struct sockaddr_in address;
} Client;

Client clientBuffer[MAX_CLIENTS];
fd_set fileDescriptors;

void initializeClientBuffer() {
    for (unsigned int i = 0; i < MAX_CLIENTS; i++) {
        clientBuffer[i].used = false;
    }
}

/**
 * Funkcija, kuri paruošia failu descriptorius select funkcijai.
 * Gražina maxfileDescriptor kuris naudojamas select'e.
 *
 * @param server_socket
 * @return
 */
int prepareFileDescriptorSetForSelect(SOCKET server_socket) {
    int maxfd = server_socket;

    FD_ZERO(&fileDescriptors);
    FD_SET(server_socket, &fileDescriptors);

    for (int i = 0; i < MAX_CLIENTS; i++) {
        if (clientBuffer[i].used) {
            FD_SET(clientBuffer[i].socket, &fileDescriptors);

            if (clientBuffer[i].socket > maxfd)
                maxfd = clientBuffer[i].socket;
        }
    }

    return maxfd;
}

/**
 * Funkcija skirta spausdinimui į ekraną.
 */
void write_log(const char *message, ...) {
    va_list args;
    va_start(args, message);

    vprintf(message, args);

    va_end(args);

    printf("\n");
}

/**
 * Funkcija skirta initializuoti winsock bibliotekai.
 *
 * @returns bool - 1 success
 */
bool initializeWinsock(WSADATA *wsa) {
    write_log("Initializing winsock library...");

    if (WSAStartup(MAKEWORD(2, 2), wsa) != 0) {
        write_log("Failed to initialize winsock library!");
        write_log("Error code: %d", WSAGetLastError());

        return false;
    }

    write_log("Winsock library successfully initialized!");

    return true;
}

/**
 * Funkcija skirta sukurti socket'ui
 *
 * @returns SOCKET - INVALID_SOCKET on failure
 */
SOCKET createSocket() {
    write_log("Creating socket...");

    SOCKET s = socket(AF_INET, SOCK_STREAM, 0);

    if (s == INVALID_SOCKET) {
        write_log("Failed to create a socket!");
        write_log("Error code: %d", WSAGetLastError());

        return s;
    }

    write_log("Socket %u was successfully created!", s);

    //Will set socket to allow multiple connections
    int opt = TRUE;
    int optres = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const char *) &opt, sizeof(opt));

    if (optres < 0) {
        write_log("Failed to set socket support multiple connections!");
        write_log("Error code: %d", WSAGetLastError());
        closesocket(s);

        return INVALID_SOCKET;
    }

    return s;
}

void printAddress(struct sockaddr_in const *address);

/**
 * Funkcija skirta prijungti socketą prie serverio.
 *
 * @param socket
 * @param address
 * @return bool - true on success.
 */
bool bindSocketToAddress(SOCKET socket, struct sockaddr_in const *address) {
    write_log("Binding %u socket to:", socket);
    printAddress(address);

    int result = bind(socket, (const struct sockaddr *) address, sizeof(struct sockaddr_in));
    if (result == SOCKET_ERROR) {
        write_log("Failed to bind the socket!");
        write_log("Error code: %d", WSAGetLastError());

        return false;
    }

    write_log("Successfully binded!");

    return true;
}

/**
 * Funkcija skirta padaryti socketą klausytis prisijungimų.
 *
 * @param socket
 * @param maxPendingConnections
 * @return
 */
bool makeSocketListenToConnections(SOCKET socket, int maxPendingConnections) {
    int res = listen(socket, 3);

    if (res < 0) {
        write_log("Could not make socket %u to listen for incomming conections!", socket);
        write_log("Error code: %d", WSAGetLastError());

        return false;
    }

    write_log("Socket %u is listening for new connections!", socket);

    return true;
}

/**
 * Finds first unused client id.
 * Otherwise returns -1
 *
 * @return clientID
 */
int findUnusedClientID() {
    for (int i = 0; i < MAX_CLIENTS; i++) {
        if (!clientBuffer[i].used)
            return i;
    }

    return -1;
}

/**
 * Funkcija skirta socket'ui priimti prisijungimą.
 *
 * @param socket
 * @return
 */
int makeSocketAcceptConnection(SOCKET socket) {
    int clientID = findUnusedClientID();

    if (clientID == -1)
        return clientID;

    int clientAddressSize = sizeof(clientBuffer[clientID].address);
    clientBuffer[clientID].socket = accept(socket, (struct sockaddr *) &(clientBuffer[clientID].address),
                                           &clientAddressSize);

    if (clientBuffer[clientID].socket == INVALID_SOCKET) {
        write_log("Failed to accept client socket!");
        write_log("Error code: %d", WSAGetLastError());
    }

    clientBuffer[clientID].used = true;

    write_log("Client (%d) connected!", clientID);
    printAddress(&(clientBuffer[clientID].address));

    return clientID;
}

/**
 * Funckija skirta nuskaityti direktorijos failus.
 *
 * @param directory
 * @param buffer
 * @param n
 * @return
 */
bool getFilesInDirectory(const char *directory, char *buffer, unsigned int n) {
    WIN32_FIND_DATA fdFile;
    HANDLE hFind = NULL;

    char path[2048] = "";
    sprintf(path, "%s\\*.*", directory);

    if ((hFind = FindFirstFile(path, &fdFile)) == INVALID_HANDLE_VALUE) {
        return false;
    }

    do {
        if (strcmp(fdFile.cFileName, ".") != 0
            && strcmp(fdFile.cFileName, "..") != 0) {
            if (strlen(buffer) == 0)
                sprintf_s(buffer, n, "%s", fdFile.cFileName);
            else
                sprintf_s(buffer, n, "%s\r\n%s", buffer, fdFile.cFileName);
        }
    } while (FindNextFile(hFind, &fdFile));

    FindClose(hFind);

    return true;
}

/**
 * Funkcija skirta suhandlinti kliento atsiusta info.
 *
 * @param clientID
 * @param message
 */
void handleClientMessage(int clientID, const char *message) {
    write_log("Recieved message from client %d", clientID);

    Client *client = &(clientBuffer[clientID]);

    if (strcmp(message, "list") == 0) {
        write_log("Client wanted to list all available files!");

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        getFilesInDirectory("data", buffer, sizeof(buffer));

        if(strlen(buffer) == 0)
        {
            strcpy(buffer, "Server directory is empty!");
        }

        send(clientBuffer[clientID].socket, buffer, (int) strlen(buffer), 0);
    } else if (strncmp(message, "get|", 4) == 0) {
        char filename[128];
        strcpy(filename, message + 4);

        write_log("Client wanted to download %s file!", filename);

        char fullFilename[256];
        sprintf_s(fullFilename, 256, "data\\%s", filename);

        FILE *file = fopen(fullFilename, "rb");
        if (file == NULL) {
            write_log("File was not found!");
            send(clientBuffer[clientID].socket, "0", 1, 0);
            return;
        }

        fseek(file, 0L, SEEK_END);
        long size = ftell(file);
        fseek(file, 0L, SEEK_SET);

        write_log("File was found and weighs %ld bytes!", size);

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        sprintf_s(buffer, sizeof(buffer), "%ld", size);
        send(clientBuffer[clientID].socket, buffer, (int) strlen(buffer), 0);

        for (long i = 0; i < size;) {
            memset(buffer, 0, sizeof(buffer));
            size_t read = fread(buffer, sizeof(char), 1024, file);
            send(clientBuffer[clientID].socket, buffer, read, 0);
            i += read;

            write_log("Sent %ld bytes to client!", read);
        }

        fclose(file);
    } else if (strncmp(message, "upload|", 7) == 0) {
        char filename[128];
        strcpy(filename, message + 7);

        char fullFilename[256];
        sprintf_s(fullFilename, 256, "data\\%s", filename);

        char buffer[1024];
        memset(buffer, 0, sizeof(buffer));
        recv(clientBuffer[clientID].socket, buffer, sizeof(buffer), 0);

        long size = atol(buffer);
        write_log("Client wanted to upload %s file (%ld bytes)!", filename, size);

        FILE* file = fopen(fullFilename, "wb");
        for(long i = 0; i < size;)
        {
            memset(buffer, 0, sizeof(buffer));
            int read = recv(clientBuffer[clientID].socket, buffer, sizeof(buffer), 0);
            fwrite(buffer, sizeof(char), read, file);
            i += read;

            write_log("Read %d bytes", read);
        }

        fclose(file);
        write_log("%s uploaded!", filename);
    }
}

/**
 * Funckija skirta suhandlinti kai klientas atsijungia.
 *
 * @param clientID
 */
void handleDisconnect(int clientID) {
    closesocket(clientBuffer[clientID].socket);

    clientBuffer[clientID].used = false;
    clientBuffer[clientID].socket = -1;
    memset(&(clientBuffer[clientID].address), 0, sizeof(clientBuffer[clientID].address));

    write_log("Client (%d) disconnected!", clientID);
}

/**
 * Funkcija skirta paprašyti vartotojo įvesti hostinimo adresą
 */
struct sockaddr_in requestAddressFromConsoleInput() {
    struct sockaddr_in result = {};
    result.sin_family = AF_INET;
    result.sin_addr.s_addr = INADDR_ANY;

    while (true) {
        char buffer[20];

        write_log("Enter Port:");
        scanf("%s", buffer);
        char *end;
        long temp = strtol(buffer, &end, 10);

        if (temp < 0 || temp > USHRT_MAX || end == buffer || (*end) != '\0') {
            write_log("Incorrect port format!");
            continue;
        }

        result.sin_port = temp;
        return result;
    }
}

/**
 * Funkcija skirta atspausdinti adresa i konsole.
 *
 * @param address
 */
void printAddress(struct sockaddr_in const *address) {
    char *str = inet_ntoa(address->sin_addr);

    write_log("IP: %s", str);
    write_log("Port: %hu", address->sin_port);
}

/**
 * Funkcija skirta patikrinti ar aplankas egzistuoja.
 *
 * @param szPath
 * @return
 */
BOOL DirectoryExists(LPCTSTR szPath) {
    DWORD dwAttrib = GetFileAttributes(szPath);

    return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
            (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

int main(int argc, char *argv[]) {
    initializeClientBuffer();

    if (!DirectoryExists("data")) {
        if (!CreateDirectoryA("data", NULL)) {
            write_log("Failed to create directory data!");
            write_log("Error code: %d", GetLastError());
        }
    }

    WSADATA wsa;
    if (!initializeWinsock(&wsa))
        return -1;

    SOCKET socket = createSocket();
    if (socket == INVALID_SOCKET)
        return -1;

    while (true) {
        struct sockaddr_in address = requestAddressFromConsoleInput();
        if (bindSocketToAddress(socket, &address))
            break;
    }

    makeSocketListenToConnections(socket, 5);

    while (true) {
        int maxFD = prepareFileDescriptorSetForSelect(socket);
        int res = select(maxFD + 1, &fileDescriptors, NULL, NULL, NULL);

        if (res < 0 && errno != EINTR) {
            write_log("select function encountered an error!");
            write_log("Error code: %d", WSAGetLastError());
            return -1;
        }

        if (FD_ISSET(socket, &fileDescriptors)) {
            makeSocketAcceptConnection(socket);
        }

        for (int i = 0; i < MAX_CLIENTS; i++) {
            if (clientBuffer[i].used) {
                if (FD_ISSET(clientBuffer[i].socket, &fileDescriptors)) {
                    char buffer[BUFFER_LENGTH];
                    memset(buffer, 0, sizeof(buffer));

                    int r_len = recv(clientBuffer[i].socket, buffer, BUFFER_LENGTH, 0);
                    if (r_len == -1 || r_len == 0)
                        handleDisconnect(i);

                    else {
                        handleClientMessage(i, buffer);
                    }
                }
            }
        }
    }

    return 0;
}