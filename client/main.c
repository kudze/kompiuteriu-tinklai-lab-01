#include <stdio.h>
#include <stdarg.h>
#include <winsock2.h>
#include <stdbool.h>
#include <limits.h>
#include <windows.h>

#define BUFFER_LENGTH 1024

/**
 * Funkcija skirta spausdinimui į ekraną.
 */
void write_log(const char* message, ...) {
    va_list args;
    va_start(args, message);

    vprintf(message, args);

    va_end(args);

    printf("\n");
}

/**
 * Funkcija skirta initializuoti winsock bibliotekai.
 * 
 * @returns bool - 1 success
 */
bool initializeWinsock(WSADATA* wsa) {
    write_log("Initializing winsock library...");

    if(WSAStartup(MAKEWORD(2, 2), wsa) != 0) {
        write_log("Failed to initialize winsock library!");
        write_log("Error code: %d", WSAGetLastError());

        return false;
    }

    write_log("Winsock library successfully initialized!");

    return true;
}

/**
 * Funkcija skirta sukurti socket'ui
 * 
 * @returns SOCKET - INVALID_SOCKET on failure
 */
SOCKET createSocket()
{
    write_log("Creating socket...");

    SOCKET s = socket(AF_INET, SOCK_STREAM, 0);

    if(s == INVALID_SOCKET)
    {
        write_log("Failed to create a socket!");
        write_log("Error code: %d", WSAGetLastError());

        return s;
    }

    write_log("Socket %u was successfully created!", s);

    return s;
}

void printAddress(struct sockaddr_in const* address);

/**
 * Funkcija skirta prijungti socketą prie serverio.
 *
 * @param socket
 * @param address
 * @return bool - true on success.
 */
bool connectSocketToAddress(SOCKET socket, struct sockaddr_in const* address)
{
    write_log("Connecting %u socket to:", socket);
    printAddress(address);

    int result = connect(socket, (const struct sockaddr *) address, sizeof(struct sockaddr_in));
    if(result == SOCKET_ERROR) {
        write_log("Failed to connect to the server!");
        write_log("Error code: %d", WSAGetLastError());

        return false;
    }

    write_log("Successfully connected!");

    return true;
}

/**
 * Funkcija skirta paprašyti vartotojo įvesti prisijungimo adresą
 */
struct sockaddr_in requestAddressFromConsoleInput()
{
    struct sockaddr_in result = {};
    result.sin_family = AF_INET;

    while(true) {
        char buffer[20];

        write_log("Enter IP address:");
        scanf("%s", buffer);

        if(strcmp(buffer, "localhost") == 0) {
            result.sin_addr.s_addr = inet_addr("127.0.0.1");
        } else {
            result.sin_addr.s_addr = inet_addr(buffer);
        }

        write_log("Enter Port:");
        scanf("%s", buffer);
        char* end;
        long temp = strtol(buffer, &end, 10);

        if(temp < 0 || temp > USHRT_MAX || end == buffer || (*end) != '\0') {
            write_log("Incorrect port format!");
            continue;
        }

        result.sin_port = temp;
        return result;
    }
}

/**
 * Funkcija kuri papraso vartotojo ivesti string
 */
void requestStringFromConsoleInput(char* buffer, const char* message)
{
    write_log(message);
    scanf("%s", buffer);
}

#define USER_ACTION_LIST 0
#define USER_ACTION_GET 1
#define USER_ACTION_UPLOAD 2
#define USER_ACTION_EXIT 3

unsigned int requestActionFromCosnoleInput()
{
    while(true)
    {
        write_log("");
        write_log("Available actions:");
        write_log("* list - Lists available files");
        write_log("* get - Downloads a file");
        write_log("* upload - Uploads a file");
        write_log("* exit - Terminates program");
        write_log("");
        write_log("Requested action:");

        char buffer[20];
        scanf("%s", buffer);

        if(strcmp(buffer, "list") == 0)
        {
            return USER_ACTION_LIST;
        }

        if(strcmp(buffer, "get") == 0)
        {
            return USER_ACTION_GET;
        }

        if(strcmp(buffer, "upload") == 0)
        {
            return USER_ACTION_UPLOAD;
        }

        if(strcmp(buffer, "exit") == 0)
        {
            return USER_ACTION_EXIT;
        }
    }
}

/**
 * Funkcija skirta atspausdinti adresa i konsole.
 *
 * @param address
 */
void printAddress(struct sockaddr_in const* address)
{
    char* str = inet_ntoa(address->sin_addr);

    write_log("IP: %s", str);
    write_log("Port: %hu", address->sin_port);
}

int main(int argc, char* argv[])
{
    WSADATA wsa;
    if(!initializeWinsock(&wsa))
        return -1;

    SOCKET socket = createSocket();
    if(socket == INVALID_SOCKET)
        return -1;

    while(true) {
        struct sockaddr_in address = requestAddressFromConsoleInput();
        if (connectSocketToAddress(socket, &address))
            break;
    }

    while(true) {
        unsigned char action = requestActionFromCosnoleInput();

        if(action == USER_ACTION_LIST)
        {
            send(socket, "list", sizeof("list"), 0);

            char buffer[1024];
            memset(buffer, 0, sizeof(buffer));
            recv(socket, buffer, sizeof(buffer), 0);

            write_log("Server's files:");
            write_log("%s", buffer);
            continue;
        }

        if(action == USER_ACTION_GET)
        {
            char filename[128];
            memset(filename, 0, sizeof(filename));
            requestStringFromConsoleInput(filename, "Enter filename:");

            char buffer[1024];
            memset(buffer, 0, sizeof(buffer));
            int written = sprintf_s(buffer, sizeof(buffer), "get|%s", filename);
            send(socket, buffer, written, 0);

            memset(buffer, 0, sizeof(buffer));
            recv(socket, buffer, sizeof(buffer), 0);
            if(strcmp(buffer, "0") == 0) {
                write_log("File does not exist!");
                continue;
            }

            long size = atol(buffer);
            write_log("Downloading %s (%ld bytes)", filename, size);

            FILE* file = fopen(filename, "wb");
            for(long i = 0; i < size;)
            {
                memset(buffer, 0, sizeof(buffer));
                int read = recv(socket, buffer, sizeof(buffer), 0);
                fwrite(buffer, sizeof(char), read, file);
                i += read;

                write_log("Read %d bytes", read);
            }

            fclose(file);
            write_log("%s downloaded!", filename);
        }

        if(action == USER_ACTION_UPLOAD)
        {
            char filename[128];
            memset(filename, 0, sizeof(filename));
            requestStringFromConsoleInput(filename, "Enter filename:");

            FILE* file = fopen(filename, "rb");
            if(file == NULL) {
                write_log("File does not exist!");
                continue;
            }

            char buffer[1024];
            memset(buffer, 0, sizeof(buffer));
            int written = sprintf_s(buffer, sizeof(buffer), "upload|%s", filename);
            send(socket, buffer, written, 0);

            fseek(file, 0L, SEEK_END);
            long size = ftell(file);
            fseek(file, 0L, SEEK_SET);

            write_log("File was found and weighs %ld bytes!", size);

            memset(buffer, 0, sizeof(buffer));
            sprintf_s(buffer, sizeof(buffer), "%ld", size);
            send(socket, buffer, (int) strlen(buffer), 0);

            for(long i = 0; i < size;)
            {
                memset(buffer, 0, sizeof(buffer));
                size_t read = fread(buffer, sizeof(char), 1024, file);
                send(socket, buffer, read, 0);
                i += read;

                write_log("Sent %ld bytes to server!", read);
            }

            fclose(file);
        }

        if(action == USER_ACTION_EXIT)
        {
            write_log("Good evening!");
            break;
        }
    }

    closesocket(socket);
    WSACleanup();

    return 0;
}